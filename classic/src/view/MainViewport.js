Ext.define("App.view.MainViewport", {
    extend: "Ext.container.Viewport",
    xtype: "MainViewport",
    controller: "MainViewportController",
    layout: {
        type: "hbox"
    },
    items: [{
        xtype: "panel",
        title: "camera !",
        flex: 1,
        layout: {
            type: "vbox",
            align: "center",
            pack: "center"
        },
        items: {
            xtype: "image",
            width: 640,
            height: 480,
            src: "http://192.168.1.6/videostream.cgi"
        }
    },{
        xtype: "CameraControl",
        width: 200
    }]
});
