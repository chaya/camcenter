Ext.define("App.view.CameraControl", {
    extend: "Ext.panel.Panel",
    xtype: "CameraControl",
    controller: "CameraControlController",
    title: "Contrôle",
    layout: {
        type: "vbox",
        align: "center",
        pack: "center"
    },
    items: [{
        xtype: "combo",
        store: "CameraStore",
        valueField: "address",
        displayField: "name",
        forceSelection: true
    },{
        xtype: "button",
        iconCls: "fa fa-arrow-up",
        width: 50,
        listeners: {
            render: "initUpButton"
        }
    },{
        xtype: "container",
        items: [{
            xtype: "button",
            iconCls: "fa fa-arrow-left",
            width: 50,
            listeners: {
                render: "initLeftButton"
            }
        },{
            xtype: "button",
            iconCls: "fa fa-arrow-right",
            width: 50,
            listeners: {
                render: "initRightButton"
            }
        }]
    },{
        xtype: "button",
        iconCls: "fa fa-arrow-down",
        width: 50,
        listeners: {
            render: "initDownButton"
        }
    },{
        xtype: "container",
        margin: "10 0 0 0",
        items: [{
            xtype: "button",
            iconCls: "fa fa-lightbulb-o",
            style: {
                color: "#25FF00"
            },
            width: 50,
            listeners: {
                click: "lightOn"
            }
        },{
            xtype: "button",
            iconCls: "fa fa-lightbulb-o",
            style: {
                color: "#FF0000"
            },
            width: 50,
            listeners: {
                click: "lightOff"
            }
        }]
    },{
        xtype: "container",
        margin: "10 0 0 0",
        items: [{
            xtype: "button",
            iconCls: "fa fa-street-view",
            width: 50,
            listeners: {
                click: "goPosition1"
            }
        },{
            xtype: "button",
            iconCls: "fa fa-save",
            width: 50,
            listeners: {
                click: "savePosition1"
            }
        }]
    },{
        xtype: "container",
        margin: "10 0 0 0",
        items: [{
            xtype: "button",
            iconCls: "fa fa-street-view",
            width: 50,
            listeners: {
                click: "goPosition2"
            }
        },{
            xtype: "button",
            iconCls: "fa fa-save",
            width: 50,
            listeners: {
                click: "savePosition2"
            }
        }]
    },{
        xtype: "container",
        margin: "10 0 0 0",
        items: [{
            xtype: "button",
            iconCls: "fa fa-street-view",
            width: 50,
            listeners: {
                click: "goPosition3"
            }
        },{
            xtype: "button",
            iconCls: "fa fa-save",
            width: 50,
            listeners: {
                click: "savePosition3"
            }
        }]
    },{
        xtype: "container",
        margin: "10 0 0 0",
        items: [{
            xtype: "button",
            iconCls: "fa fa-street-view",
            width: 50,
            listeners: {
                click: "goPosition4"
            }
        },{
            xtype: "button",
            iconCls: "fa fa-save",
            width: 50,
            listeners: {
                click: "savePosition4"
            }
        }]
    }]
});