Ext.define("App.model.CameraModel", {
    extend: "Ext.data.Model",
    model: "CameraModel",
    fields: [
        "address",
        "name",
        "username",
        "password"
    ],
    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "items"
        }
    }
});
