Ext.define("App.view.CameraControlController", {
    extend: "Ext.app.ViewController",
    alias: "controller.CameraControlController",

    init: function() {
        "http://192.168.1.6/get_camera_params.cgi"
    },

    initUpButton: function(button) {
        button.mon(button.getEl(), {
            mousedown: function() {
                Ext.data.JsonP.request({
                    url: "http://192.168.1.6/decoder_control.cgi?command=0"
                });
            },
            mouseup: function() {
                Ext.data.JsonP.request({
                    url: "http://192.168.1.6/decoder_control.cgi?command=1"
                });
            }
        });
    },

    initLeftButton: function(button) {
        button.mon(button.getEl(), {
            mousedown: function() {
                Ext.data.JsonP.request({
                    url: "http://192.168.1.6/decoder_control.cgi?command=4"
                });
            },
            mouseup: function() {
                Ext.data.JsonP.request({
                    url: "http://192.168.1.6/decoder_control.cgi?command=1"
                });
            }
        });
    },

    initRightButton: function(button) {
        button.mon(button.getEl(), {
            mousedown: function() {
                Ext.data.JsonP.request({
                    url: "http://192.168.1.6/decoder_control.cgi?command=6"
                });
            },
            mouseup: function() {
                Ext.data.JsonP.request({
                    url: "http://192.168.1.6/decoder_control.cgi?command=1"
                });
            }
        });
    },

    initDownButton: function(button) {
        button.mon(button.getEl(), {
            mousedown: function() {
                Ext.data.JsonP.request({
                    url: "http://192.168.1.6/decoder_control.cgi?command=2"
                });
            },
            mouseup: function() {
                Ext.data.JsonP.request({
                    url: "http://192.168.1.6/decoder_control.cgi?command=1"
                });
            }
        });
    },

    lightOn: function() {
        Ext.data.JsonP.request({
            url: "http://192.168.1.6/set_misc.cgi?led_mode=2"
        });
    },

    lightOff: function() {
        Ext.data.JsonP.request({
            url: "http://192.168.1.6/set_misc.cgi?led_mode=1"
        });
    },

    savePosition1: function() {
        Ext.data.JsonP.request({
            url: "http://192.168.1.6/decoder_control.cgi?command=30"
        });

    },

    goPosition1: function() {
        Ext.data.JsonP.request({
            url: "http://192.168.1.6/decoder_control.cgi?command=31"
        });

    },

    savePosition2: function() {
        Ext.data.JsonP.request({
            url: "http://192.168.1.6/decoder_control.cgi?command=32"
        });

    },

    goPosition2: function() {
        Ext.data.JsonP.request({
            url: "http://192.168.1.6/decoder_control.cgi?command=33"
        });

    },

    savePosition3: function() {
        Ext.data.JsonP.request({
            url: "http://192.168.1.6/decoder_control.cgi?command=34"
        });

    },

    goPosition3: function() {
        Ext.data.JsonP.request({
            url: "http://192.168.1.6/decoder_control.cgi?command=35"
        });

    },

    savePosition4: function() {
        Ext.data.JsonP.request({
            url: "http://192.168.1.6/decoder_control.cgi?command=36"
        });

    },

    goPosition4: function() {
        Ext.data.JsonP.request({
            url: "http://192.168.1.6/decoder_control.cgi?command=37"
        });

    }
});
