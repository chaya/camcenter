Ext.define("App.Application", {
    extend: "Ext.app.Application",
    
    name: "App",

    models: [
        "CameraModel"
    ],

    stores: [
        "CameraStore"
    ],

    views: [
        "CameraControl",
        "CameraControlController",
        "MainViewport",
        "MainViewportController"
    ],
    
    launch: function () {
        // TODO - Launch the application
    },

    onAppUpdate: function () {
        Ext.Msg.confirm("Application Update", "This application has an update, reload?",
            function (choice) {
                if (choice === "yes") {
                    window.location.reload();
                }
            }
        );
    }
});
